# SPDX-FileCopyrightText: 2022 Carina Haupt
#
# SPDX-License-Identifier: MIT

"""
This script uses data from the booking tool Proyector to create tech lists and moderator cards for the content team.
"""
import copy

import click
import urllib.request, json
import datetime as dt
import csv

from py3o.template import Template


EXPORT_URL = "https://booking.kulkos.net/api/program/content.json?feed_token" \
             "=53909ef4c4e8c608d740b88749ed04819f9fdc627f13b6968ad2e1e17ed66f4d&event_id="
ID_FUSION_2022 = "28"
ID_FUSION_2023 = "31"
ID_FUSION_2024 = "34"


@click.command()
@click.argument("fusion_id", default=ID_FUSION_2024)
@click.argument("overview", default=True)
@click.argument("tec_list", default=False)
@click.argument("booking_id", default=0)
def run(fusion_id, overview, tec_list, booking_id):
    data = load_json(fusion_id)
    parse_json(data, overview, tec_list, booking_id)


def load_json(fusion_id):
    json_data = None
    with urllib.request.urlopen(EXPORT_URL + fusion_id) as url:
        json_data = json.loads(url.read().decode())
    return json_data


def parse_json(json_data, print_overview, print_tec_list, booking_id):
    content_data = list()

    print('Found ' + str(len(json_data)) + ' bookings.')
    if len(json_data) == 0:
        print('Nothing to do.')
    else:
        # If no ID is given, parse all bookings as set cards and create tec list
        if booking_id == 0:
            for booking in json_data:
                booking_data = parse_booking(booking)
                if booking_data is not None:
                    content_data = content_data + booking_data
            if print_overview:
                render_overview(content_data)
            if print_tec_list:
                render_teclist(content_data)
        # If ID is given, skip all other set cards and do not update tec list - this shall mainly be used for debugging
        else:
            for booking in json_data:
                if booking_id == booking['booking_id']:
                    parse_booking(booking)


def parse_booking(booking):
    booking_data = list()
    show_data = DataObject()
    show_data.booking_id = booking['booking_id']
    print('Process booking ' + str(show_data.booking_id) + '.')

    try:
        # One booking can have multiple shows in proyektor.
        shows = booking['shows']
        if len(shows) == 0:
            raise ValueError('Booking has ' + str(len(shows)) + ' shows. Expected at least 1.')

        show_data.artist_name = get_value(booking, 'program_name')
        if show_data.artist_name == "":
            show_data.artist_name = get_value(booking, 'artist_name')
        show_data.artist_count = get_value(booking, 'artist_count')
        show_data.artist_anecdote = get_value(booking, 'editorial_note')
        show_data.crew_count = get_value(booking, 'crew_count')
        show_data.program_name = get_value(booking, 'artist_name')
        show_data.program_type = get_value(booking, 'genre')

        text, lang_data = parse_lang_template(get_value(booking, 'description_de'))
        show_data.language = get_value(lang_data, 'language')
        show_data.recording = get_value(lang_data, 'recording')
        show_data.translation = get_value(lang_data, 'translation')
        if 'DE' not in show_data.language:
            text, lang_data = parse_lang_template(get_value(booking, 'description_en'))
        show_data.description = text

        for show in shows:
            start = parse_datetime(get_value(show, 'start'))
            end = parse_datetime(get_value(show, 'end'))
            show_data.date = start.date()
            show_data.start = start.time()
            show_data.end = end.time()
            show_data.duration = end - start

            # render set card
            render_setcard(show_data)
            booking_data.append(copy.deepcopy(show_data))

        return booking_data
    except Exception as e:
        print('>>> Something went wrong processing booking ' + str(show_data.booking_id))
        print(e)
        print('<<<')
        return None


def get_value(dic, key):#
    value = '?'
    if key not in dic:
        print('Key not found in data: ' + key + '. Value set to ?.')
    else:
        value = dic[key]
        if type(value) == str:
            value = value.strip()
        elif type(value) is None:
            print('Key in data: ' + key + '. But value set to null. Instead value set to ?.')
            value = '?'
    return value


def render_setcard(booking_data):
    print('Create set card for ' + str(booking_data.booking_id) + '...')
    t = Template('template/setcard_template.odt', 'output/setcard_' + str(booking_data.date) + '_'
                 + str(booking_data.start) + '_' + booking_data.artist_name + '.odt')
    data = dict(booking_data=booking_data)
    t.render(data)
    print('...done.')


def render_overview(content_data):
    with open('output/overview.csv', 'w', newline='') as tech_file:
        wr = csv.writer(tech_file, quoting=csv.QUOTE_ALL)
        wr.writerow(vars(content_data[0]).keys())
        for booking in content_data:
            wr.writerow(vars(booking).values())


def render_teclist(content_data):
    print('Create tech list...')

    rows_per_day = {}
    for booking in content_data:
        if booking.date not in rows_per_day:
            rows_per_day[booking.date] = []

        tec_data = [booking.date,
                    booking.start,
                    booking.end,
                    booking.duration,
                    booking.program_name,
                    booking.artist_name,
                    booking.program_type,
                    booking.translation,
                    booking.recording]
        rows_per_day[booking.date].append(tec_data)
        tec_data = [booking.tec_mics,
                    booking.tec_laptop_needed,
                    booking.tec_video_connector,
                    booking.tec_sound_needed,
                    booking.tec_sound_input,
                    booking.tec_film_played,
                    booking.tec_film_input,
                    booking.tec_film_format,
                    booking.tec_misc]
        rows_per_day[booking.date].append(tec_data)
        rows_per_day[booking.date].append(['---'] * 6)

        with open('output/techlist.csv', 'w', newline='') as tech_file:
            wr = csv.writer(tech_file, quoting=csv.QUOTE_ALL)
            write_csv_header(wr)
            for date, rows in rows_per_day.items():
                with open('output/techlist_' + str(date) + '.csv', 'w', newline='') as tech_day_file:
                    wr_day = csv.writer(tech_day_file, quoting=csv.QUOTE_ALL)
                    write_csv_header(wr)
                    wr_day.writerows(rows)

                wr.writerows(rows)

    print('...done.')


def write_csv_header(wr):
    wr.writerow(['Date', 'Start', 'End', 'Duration', 'Title', 'Artist', 'Type', 'Translation', 'Recording'])
    wr.writerow(['#Mics', 'Laptop Needed', 'Video Connector', 'Sound Needed', 'Sound Input', 'Film Played',
                 'Film Input', 'Film Format', 'Misc'])


def parse_lang_template(lang_field):
    text, lang_string = get_description_text_string(lang_field)
    lang_data = dict()
    if 'Language' not in lang_string or 'Translation' not in lang_string or 'Recording' not in lang_string:
        print('Not matching language template.')
    lang_parts = lang_string.split(':')
    lang_data['language'] = lang_parts[1].replace(' Translation', '').strip()
    lang_data['translation'] = lang_parts[2].replace(' Recording', '').strip()
    lang_data['recording'] = lang_parts[3].strip()
    return text, lang_data


def get_description_text_string(lang_field):
    entries = lang_field.split('\r\n')
    text = '\r\n'.join(entries[0:-1])
    lang_string = entries[-1]
    return text, lang_string


def parse_tec_template(tec_template):
    print(tec_template)
    tec_data = dict()
    tec_template_entries = tec_template.split('\r\n')
    for entry in tec_template_entries:
        if '=' not in entry:
            continue
        e = entry.split('=', 1)
        tec_data[e[0].strip()] = e[1].strip().replace('"', '')
    if len(tec_data) == 0:
        print('Not matching tec template: Ignoring field "Redaktionshinweis"')
    return tec_data


def parse_datetime(date_string: str):
    return dt.datetime.fromisoformat(date_string)


class DataObject(object):
    pass


if __name__ == '__main__':
    run()
