# SPDX-FileCopyrightText: 2022 Carina Haupt
#
# SPDX-License-Identifier: CC0-1.0

# Run: pip install -r requirements.txt
click >= 7.0
py3o.template

