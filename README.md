# Proyector 2 Content

This script uses data from the booking tool Proyector to create tech lists and moderator cards for the content team. 
It uses therefore a special export for the content team provided by the Proyector developers. The results can be found 
in the `output` folder. 

## Installation

- Get the code
- Install Python >= 3.9
- Install the dependencies: `pip install -r requirements.txt`
  - [click](https://click.palletsprojects.com) licensed under BSD-3-Clause
  - [py3o.template](https://pypi.org/project/py3o.template/) licensed under MIT

## Usage

Manual execution:
```
python proyector2content.py <event_id> <booking_id>
```
`event_id` and `booking_id` are both optional. 
* If no `event_id` is given, the id of Fusion 2022 is taken.
* If no `booking_id` is given, all bookings are parsed. *ATTENTION*: If a booking_id is provided, no tech lists are 
created, since these require parsing all bookings. This feature is more for debugging single set card data.

Automatic execution:

[GitLab](https://gitlab.berlin.ccc.de/duckattack/proyector2content/) runs the script every full hour, resulting in a 
fresh set of set cards and tech lists. The latest set can be downloaded here: [Setcards & Techlist as zip](https://gitlab.berlin.ccc.de/duckattack/proyector2content/-/jobs/artifacts/master/download?job=create_setcards_and_techlist)

For information about occurred issues (missing data, skipped bookings, etc.) go to [Jobs](https://gitlab.berlin.ccc.de/duckattack/proyector2content/-/jobs) 
and click on the status button of the item top most in the list which states `passed`.

If you have access to the project, you can also run the Job manually via Gitlab. Therefore, go to [Schedules](https://gitlab.berlin.ccc.de/duckattack/proyector2content/-/pipeline_schedules) and press the play button.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
